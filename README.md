# Info Science Environnement Société

Espace de partage pour le groupe de réflexion Science Environnement Société de Grenoble Rhône-Alpes

* [Info pratiques](https://gitlab.inria.fr/science-environnement-societe/info/-/wikis/info_pratiques)
* [Annonces de Séminaires](https://gitlab.inria.fr/science-environnement-societe/info/-/wikis/seminaires)
* [Ressources](https://gitlab.inria.fr/science-environnement-societe/info/-/wikis/ressources)
* [Compte-rendu des réunions](https://gitlab.inria.fr/science-environnement-societe/info/-/wikis/compte_rendu%20des%20réunions)
